<?php

$config = [

    // This is a authentication source which handles admin authentication.
    'admin' => [
        // The default is to use core:AdminPassword, but it can be replaced with
        // any authentication source.

        'core:AdminPassword',
    ],


    // An authentication source which can authenticate against both SAML 2.0
    // and Shibboleth 1.3 IdPs.
    'default-sp' => [
        'saml:SP',
	'privatekey' => 'saml.pem',
        'certificate' => 'saml.crt',
	'privatekey_pass' => 'N7?k@pW*e63tud#W',
        // The entity ID of this SP.
        // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
        'entityID' => 'umassamherstgatewaydev.prod.acquia-sites.com',

        // The entity ID of the IdP this SP should contact.
        // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
        #'idp' => 'https://shibdev3.idmintegration.com/idp/shibboleth',
        'idp' => 'https://webauth.umass.edu/idp/shibboleth',
    ],

    // Umass Gateway Acquia "dev"
    // Accessible at "https://umassamherstgatewaydev.prod.acquia-sites.com"
    'umassamherstgateway-dev' => [
      'saml:SP',
      'privatekey' => 'saml.pem',
      'certificate' => 'saml.crt',
      'privatekey_pass' => 'N7?k@pW*e63tud#W',
      'entityID' => 'umassamherstgatewaydev.prod.acquia-sites.com',
      'idp' => 'https://webauth.umass.edu/idp/shibboleth',
    ],

    // Umass Gateway Acquia "test" / staging
    // Accessible at "https://umassamherstgatewaystg.prod.acquia-sites.com"
    'umassamherstgateway-staging' => [
      'saml:SP',
      'privatekey' => 'saml.pem',
      'certificate' => 'saml.crt',
      'privatekey_pass' => 'N7?k@pW*e63tud#W',
      'entityID' => 'umassamherstgatewaystg.prod.acquia-sites.com',
      'idp' => 'https://webauth.umass.edu/idp/shibboleth',
    ],

    // Umass Gateway Acquia "prod"
    // Accessible at "https://umassamherstgateway.prod.acquia-sites.com"
    'umassamherstgateway-prod' => [
      'saml:SP',
      'privatekey' => 'saml.pem',
      'certificate' => 'saml.crt',
      'privatekey_pass' => 'N7?k@pW*e63tud#W',
      'entityID' => 'umassamherstgateway.prod.acquia-sites.com',
      'idp' => 'https://webauth.umass.edu/idp/shibboleth',
    ],

    // Umass Gateway Acquia "prod" www domain via Akamai
    // Accessible at "https://www.umass.edu"
    'www-umass-edu' => [
      'saml:SP',
      'privatekey' => 'saml.pem',
      'certificate' => 'saml.crt',
      'privatekey_pass' => 'N7?k@pW*e63tud#W',
      'entityID' => 'acquia-www.umass.edu',
      'idp' => 'https://webauth.umass.edu/idp/shibboleth',
    ],

    // Umass Gateway Acquia "prod" EDIT domain via Akamai
    // Accessible at "https://edit.umass.edu"
    'edit-umass-edu' => [
      'saml:SP',
      'privatekey' => 'saml.pem',
      'certificate' => 'saml.crt',
      'privatekey_pass' => 'N7?k@pW*e63tud#W',
      'entityID' => 'acquia-edit.umass.edu',
      'idp' => 'https://webauth.umass.edu/idp/shibboleth',
    ],

];
