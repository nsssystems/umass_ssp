<?php

/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote
 */
$metadata['https://shibdev3.idmintegration.com/idp/shibboleth'] = array (
  'entityid' => 'https://shibdev3.idmintegration.com/idp/shibboleth',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
      'Location' => 'https://shibdev3.idmintegration.com/idp/profile/Shibboleth/SSO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://shibdev3.idmintegration.com/idp/profile/SAML2/POST/SSO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://shibdev3.idmintegration.com/idp/profile/SAML2/POST-SimpleSign/SSO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://shibdev3.idmintegration.com/idp/profile/SAML2/Redirect/SSO',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://shibdev3.idmintegration.com/idp/profile/SAML2/Redirect/SLO',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://shibdev3.idmintegration.com/idp/profile/SAML2/POST/SLO',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
      'Location' => 'https://shibdev3.idmintegration.com/idp/profile/SAML2/POST-SimpleSign/SLO',
    ),
    3 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://shibdev3.idmintegration.com:8443/idp/profile/SAML2/SOAP/SLO',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
      'Location' => 'https://shibdev3.idmintegration.com:8443/idp/profile/SAML1/SOAP/ArtifactResolution',
      'index' => 1,
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://shibdev3.idmintegration.com:8443/idp/profile/SAML2/SOAP/ArtifactResolution',
      'index' => 2,
    ),
  ),
  'NameIDFormats' => 
  array (
    0 => 'urn:mace:shibboleth:1.0:nameIdentifier',
    1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIDVzCCAj+gAwIBAgIUDPZNjmwrk908S97Xt3xTpPzD8qwwDQYJKoZIhvcNAQEL
BQAwJjEkMCIGA1UEAwwbc2hpYmRldjMuaWRtaW50ZWdyYXRpb24uY29tMB4XDTE1
MDUwNzE4NTc1MVoXDTM1MDUwNzE4NTc1MVowJjEkMCIGA1UEAwwbc2hpYmRldjMu
aWRtaW50ZWdyYXRpb24uY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
AQEAhtb4St8pBfhBIOpRcHnNnXRde6R6mW2vx7ZRkgGBzEWof++XXoCOs/6DqAgs
oHI9N4WARHKjQSDdRE5EEFCSKBbxuh3gfVOWSL+w1Mr3lab+H12JjH+soHPGaiDw
k0ZFm8RYg9Q1cixjec5u+Xp5klAsPeZpAq+Yko+SM1lFhvu2ZIyQxbcOJJ0wr8N5
zOZQIUG/XorJWnVfNDLsQizL3ax4JtOJmQ3syCXKXIIVA16nU704AITVPplrGSoS
JT4hfLAe2wuvzaWq/ypHVzN09whA3Bacl/631yGwY5ZZFfla5hx/9BFRdscxufuX
tNuxZfP/TyO20eNX2WmHg3D2IwIDAQABo30wezAdBgNVHQ4EFgQUHYVYkHOmAI9v
4W5AKfP9jEEWQnMwWgYDVR0RBFMwUYIbc2hpYmRldjMuaWRtaW50ZWdyYXRpb24u
Y29thjJodHRwczovL3NoaWJkZXYzLmlkbWludGVncmF0aW9uLmNvbS9pZHAvc2hp
YmJvbGV0aDANBgkqhkiG9w0BAQsFAAOCAQEAQaaaecSllUWf+88HwInpAA2NvJzC
Vliiex2HmLHcgSj6TsJBhcPtqmw7Jb86iA5GsgpvvSCaAPtpAT6vvCZflJGlJSMj
CIioWNA2/2WkvN+XpMirjeKL5vPElwDh7Lm9JznaVqrnF9U2ANVFJun1kEmKe8tH
qRtQtn6PhuahDEE1FWkPTJJ8CgZOk7zJsYUTcMnX8EsbESh0OtmCtjGXNPir4+uE
XjshOIvkeF/UY2Qi8W7xqVSZDSQzRTB11V0WuUTOKZ6785IiQxqrkpqZnnAJs2NB
4mB4gBqI50H1JcliH/jOxVfoo5Mt87tVwly/kP8gtV6cLZjAP6pct3xt4g==
                        ',
    ),
    1 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIDVzCCAj+gAwIBAgIUTcKfExi8Z9bJrK64tzjI2CZ0Z8UwDQYJKoZIhvcNAQEL
BQAwJjEkMCIGA1UEAwwbc2hpYmRldjMuaWRtaW50ZWdyYXRpb24uY29tMB4XDTE1
MDUwNzE4NTc0OFoXDTM1MDUwNzE4NTc0OFowJjEkMCIGA1UEAwwbc2hpYmRldjMu
aWRtaW50ZWdyYXRpb24uY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
AQEAnWoQx+O5hu5lVN3V6uP2XD8DMsm0RzEshR4+dWz4NPIjZWFYdAjWqayNBMgM
Pgi1PZ4aczTO8B6sO3iKTNI2YC/C1SkBk35+nzG88kvqb4P1IE8ryupmABhYS6Oq
Kx05ueLfwF174Q0sHzaiESxNa0Nz4BmmBW7D/R0B5gt3vg0mPX0RUyzXz2mw7rK2
CQ0qFVQ7FLxRx8t8IlLk62sbmitSPl9uTmNRQEMwc17Ed8Hml/sn1Vf/HdQmtsKy
HuTpGJTe0IJRurpt/S3QFW/a77xyMWRzq6tm6ORgne1DC502ShuH+tZ7n7Ua6Dml
9bhub5TVrcvI8f4XoEtpUTZleQIDAQABo30wezAdBgNVHQ4EFgQUqr0riUx0otcU
fHZWzz+BiEhePDowWgYDVR0RBFMwUYIbc2hpYmRldjMuaWRtaW50ZWdyYXRpb24u
Y29thjJodHRwczovL3NoaWJkZXYzLmlkbWludGVncmF0aW9uLmNvbS9pZHAvc2hp
YmJvbGV0aDANBgkqhkiG9w0BAQsFAAOCAQEAAjBC9IR1N5tKTXX7+7FUop+rBWq6
qajwdmJSn88hcjlvLFsO1KmDM6hnF6g+FQ+ojw+Pv6YF5sEutDUB2R1Wn6Yiqeg4
qHpGZ0Kss9jf02z0QqICgcGzLnVLp1lXbr9O0Ne1urCW4L2QUG9bRs2l+4oUsB1i
tyiQFFjgcxZDT7enPm4XJGmcQtnB/Fh3gUxoworF9Gj+XOVH2FmvdCNQIIe5c87f
OPWn5hjJv03Wuf72wa5k3ih17X9+VTTuUtEYWKMuy/mavPSwau0a2KXouqWkxCdg
d2Zd36ts0fMkrNSilK9sJVHA4rJVwdmcd7RyWBr+4X9K+JazevzUTPjAag==
                        ',
    ),
    2 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIDWDCCAkCgAwIBAgIVAKOzsnzqgXhUdsGxXEFXpJqjk2hkMA0GCSqGSIb3DQEB
CwUAMCYxJDAiBgNVBAMMG3NoaWJkZXYzLmlkbWludGVncmF0aW9uLmNvbTAeFw0x
NTA1MDcxODU3NTBaFw0zNTA1MDcxODU3NTBaMCYxJDAiBgNVBAMMG3NoaWJkZXYz
LmlkbWludGVncmF0aW9uLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAJmIe2Q31d3ESbi0J13XaU/svw7370f4fqKIuJfhkJoHICeq6LbkLEczDFdu
SHlwn1WDOUlG7pW5sQYIIJDEBqD/udw3d8WlfQIr6xtxQ0BlUJd6OCo1+E8IguH5
Af5MEV7BzNRU5Vhj1lRkWZRUBiqUQQSr8AYSDJtJmksrT5KgNcvMX06gYHdbpcuL
83vSYsGtSCxEqLPfygWqi7f4SH9ndpWwNKvxvtnSjRsBlDI33IYZe1sX3FroUw8O
I/+LB31At4apTqzMQOFdWunNEviAo+UPLDATL9WqHKO9Sg1qnH/npfP03Nrmc3p2
bDOEaIqk6jlqaQMfqTL7GJJzY5sCAwEAAaN9MHswHQYDVR0OBBYEFPHztm/I1vt/
cRxVLz0gocOPVA/XMFoGA1UdEQRTMFGCG3NoaWJkZXYzLmlkbWludGVncmF0aW9u
LmNvbYYyaHR0cHM6Ly9zaGliZGV2My5pZG1pbnRlZ3JhdGlvbi5jb20vaWRwL3No
aWJib2xldGgwDQYJKoZIhvcNAQELBQADggEBAIE3Qt2WBAsGuGzkrZlGhX/ARAB9
/a7yFkyFvVLJ4FQQr3CN1RF04naPEsQ1a+xha3BwOPmVa7iK7W1uUi3Qm8fThvlZ
7BBhwgM75pvAY+DUJPuDd/CSxfTW8CjtJa8v7tzlJO2kpM1dEbYZHN6l3mqru2iT
6Hc2bCtdvQ+I48vmt0YU6yU2f/ux5kAfczI4BimGv9d7JU9IPF2HWp3KJHosC8g+
qtM1/e5tuqbB6SiAgvcbec4M6ASSICrHNg/FttQoNQsh1ExD9+kA2uatToVtlWUX
R29EROhK4c+BZIXkxCOBPAn7TpJcsqTn4jeLRqVw+3siVhToNOaDihi7po4=
                        ',
    ),
  ),
  'scope' => 
  array (
    0 => 'idmintegration.com',
  ),
  'UIInfo' => 
  array (
    'DisplayName' => 
    array (
      'en' => 'Test IdP at shibdev3',
    ),
    'Description' => 
    array (
    ),
    'InformationURL' => 
    array (
    ),
    'PrivacyStatementURL' => 
    array (
    ),
  ),
  'name' => 
  array (
    'en' => 'Test IdP at shibdev3',
  ),
);
include_once( __DIR__ . '/umass-idp.php' );
